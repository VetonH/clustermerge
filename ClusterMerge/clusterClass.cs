﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ClusterMerge
{

    /// <summary>
    /// Hole application class
    /// </summary>
    class clusterClass
    {
        /// <summary>
        /// Nested list for the fingerprints added as nested lists with 4 values for each fingerprint
        /// </summary>
        private List<List<int>> fingerprintList = new List<List<int>>();

        /// <summary>
        /// Array of merging field to put index of fingerprints
        /// </summary>
        private string[,] merge = new string[1000, 1000];

        /// <summary>
        /// Nested list of clusters, where the the clusters are later added to
        /// </summary>
        private List<List<int>> clusterList = new List<List<int>>();

        /// <summary>
        /// List to store the amount of + edges within a cluster
        /// </summary>
        private List<int> edgesInsideClusterList = new List<int>();


        /// <summary>
        /// List the total amount of edges in a cluster, both + and -
        /// </summary>
        private List<int> totalEdgesInClusterList = new List<int>();



        /// <summary>
        /// Runs the program in the right order
        /// </summary>
        public void startCluster()
        {

            Stopwatch stopwatch = Stopwatch.StartNew();
            collectFingerpint();
            //method #3
            outPrintFingerprints();
            //method #4
            countCluster();
            //method #5
            countMerge();
            //method #6
            outPrintClusters();
            //method #7
            outPrintEdges();
            
            stopwatch.Stop();
            Console.WriteLine("{0,40}{1, 5}", "\n\tTime elapsed: ", stopwatch.Elapsed);
            stopwatch.Restart();

        }//End

        /// <summary>
        /// #2 Adding differet values to the app as nested fingerprint lists
        /// </summary>
        private void collectFingerpint()
        {          
            fingerprintList.Add(new List<int> {  84    ,       100     });
            fingerprintList.Add(new List<int> {  96    ,       105     });
            fingerprintList.Add(new List<int> {  83    ,       103     });
            fingerprintList.Add(new List<int> {  100   ,       118     });
            fingerprintList.Add(new List<int> {  7     ,       33     });
            fingerprintList.Add(new List<int> {  27    ,       88     });
            fingerprintList.Add(new List<int> {  18    ,       58     });
            fingerprintList.Add(new List<int> {  24    ,       88     });
            fingerprintList.Add(new List<int> {  1     ,       73     });
            fingerprintList.Add(new List<int> {  22    ,       34     });
            fingerprintList.Add(new List<int> { 13     ,       61    });
            fingerprintList.Add(new List<int> { 4      ,       61    });
            fingerprintList.Add(new List<int> { 9      ,       70    });
            fingerprintList.Add(new List<int> { 8      ,       60    });
            fingerprintList.Add(new List<int> { 1      ,       47    });
            fingerprintList.Add(new List<int> { 12     ,       25    });
            fingerprintList.Add(new List<int> { 17     ,       64    });
            fingerprintList.Add(new List<int> { 16     ,       27   });
            fingerprintList.Add(new List<int> { 25     ,       23    });
            fingerprintList.Add(new List<int> { 5      ,       67    });

        }//End #2

        /// <summary>
        /// #3 Outprint of added fingerprints and their values
        /// </summary>
        private void outPrintFingerprints()
        {
            Console.WriteLine("\tEntered fingerprints with values: \n");


            Console.WriteLine(string.Format("{0 , 20} {1 , 10} {2 , 10}", "Fingerprint", "Value 1", "Value 2"));
            //prints out the fingerprints
            for (int t = 0; t < fingerprintList.Count; t++)
            {

                Console.Write(string.Format("{0 , 20}", "fp" + (t+1)));
                //prints out the values for each fingerprint
                for (int j = 0; j < 2; j++)
                {
                    //for every t fingerprint, prints out j values
                    Console.Write(string.Format("{0 , 11}", fingerprintList[t][j].ToString()));
                }
                Console.WriteLine("");
            }
        }//End #3

        /// <summary>
        /// #4 Algorithm to check if all values between two fingerprints are within range for merging
        /// </summary>
        private void countCluster()
        {
                //iteration for all fingerprint from 0 to last one as variable x
                for (int x = 0; x < fingerprintList.Count; x++)
                {
                    /*iteration for all fingerprints with the variable z that is above fingerprint X 
                     * and for every fingerprint X all fingerprints Z are checked*/
                    for (int z = x + 1; z < fingerprintList.Count; z++)
                    {

                        //create a variable j with a null value that will be used as a buffer for checked values in fingerprints
                        int j = 0;

                      
                            //if the values in every fingerprint on the same vector is within the value of 1 then they can be merged
                            if (Math.Abs(fingerprintList[x][0] - fingerprintList[z][0]) <= 5 || fingerprintList[x][1] == fingerprintList[z][1])
                            {
                                //variable j grows with 1 every time the values are within range and can get to maximum of 4
                                j++;
                            }

                            //if the values in every fingerprint on the same vector is within the value of 1 then they can be merged
                            if (Math.Abs(fingerprintList[x][1] - fingerprintList[z][1]) <= 15 || fingerprintList[x][1] == fingerprintList[z][1])
                            {
                                //variable j grows with 1 every time the values are within range and can get to maximum of 4
                                j++;
                            }

                    //if "j" have reached 4 then the fingerprints can be merged
                    if (j == 2)
                        {
                            //the value in the merge array gets  + edge which means they can be merged
                            merge[x, z] = "+";
                            merge[z, x] = "+";
                        }
                        //else it will be set to - edge and can not be merged
                        else
                        {
                            merge[x, z] = "-";
                            merge[z, x] = "-";
                        }
                    }
                }
        }//End #4

       
        /// <summary>
        /// #5 Create clusters of the index of each added fingerprint
        /// </summary>
        private void countMerge()
        {
                Console.Write("\n\tClustering started: ");
                //start iteration check with first value against all others
                for (int x = 0; x <= fingerprintList.Count; x++)
                {
                    //iteration for all other fingerprints above the one set as x
                    for (int z = x + 1; z < fingerprintList.Count; z++)
                    {                     
                        //if fingerprint at index x can be merged against z, which means they are withing range of 1
                        if (merge[x, z] == "+" || merge[z, x] == "+")
                        {
                            //if the value of x have not been added to another cluster yet
                            if (!clusterList.SelectMany(innerList => innerList).Any(s => s == x) && !clusterList.SelectMany(innerList => innerList).Any(s => s == z))
                            {
                                //create first cluster if none of x or z fingerprint already exist in another cluster
                                clusterList.Add(new List<int> { x, z });
                                int edge = 1;
                                edgesInsideClusterList.Add(edge);
                                totalEdgesInClusterList.Add(edge);
                            }
                            //check if fingerprint of the variable z exist in the cluster
                            else if (!clusterList.SelectMany(innerList => innerList).Any(s => s == z))
                            {                      

                                //variabel of the count of clusterList -1 to know the length to know where to put newest cluster
                                var cPos = clusterList.Count - 1;
                                //variabel of the count of list of fingerprint inside clusterList to know how far to check fingerprint z
                                var cCount = clusterList[clusterList.Count - 1].Count;
                                //create variable j that will be checked against amount of fingerprints in cluster 
                                int j = 0;
                                //runs iteration to until all fingerprints in cluster have been match against fingerprint z to see their edge
                                for (  int y = 0; y < cCount; y++)
                                {
                                    //if fingerprint in position y in nestedlist of clusterList is a merge with fingerprint z
                                    if (merge[clusterList[cPos][y], z] == "+" || merge[z, clusterList[cPos][y]] == "+")
                                    {
                                        //then variable j grows
                                        j++;
                                    }
                                }
                                //checks if most of the fingerprints in the cluster have a + edge
                                if (j > (clusterList[cPos].Count / 2))
                                {
                                    totalEdgesInClusterList[cPos] = 0;
                                    //add fingerprint z to the other fingerprints in the cluster        
                                    clusterList[cPos].Add(z);
                                    edgesInsideClusterList[cPos] += j;
                                
                                for (int w = clusterList[cPos].Count; w > 0; w--)
                                    {
                                        totalEdgesInClusterList[cPos] += clusterList[cPos].Count-w;
                                    }
                                }
                                
                            }
                        }
                    }
                }
                //x iteration as above
                for (int x = 0; x <= fingerprintList.Count; x++)
                {
                    //z iteration as above
                    for (int f = x + 1; f < fingerprintList.Count; f++)
                    {                     
                        //checks if fingerprint already exist in any other cluster
                        if (!clusterList.SelectMany(innerList => innerList).Any(s => s == f))
                        {
                            //if not then fingerprint will create own cluster
                            clusterList.Add(new List<int> { f });

                         int edge = 0;
                        edgesInsideClusterList.Add(edge);
                        totalEdgesInClusterList.Add(edge);
                        }
                    }
                }
            Console.WriteLine("\n\tClustering Finished! \n");
        }//End #5

        /// <summary>
        /// #6 prints out the result of clusters
        /// </summary>
        private void outPrintClusters()
        {
            Console.WriteLine("\tCustered Fingerprints: \n");
            //create variable to print out amount kluster
            int e = 0;
            //clusterList is the list which contains all the values
            foreach (var key in clusterList)
            {
                e++;
                Console.Write("\tCluster #c" + e + ", contain fingerprints: ");
                foreach (var keys in key)
                {                 
                    Console.Write("fp" + (keys+1) + ", ");
                }
                Console.WriteLine("\n\t Of the total amount of " + totalEdgesInClusterList[e-1] + " edges, there are " + edgesInsideClusterList[e-1] + " +edges and " +  (totalEdgesInClusterList[e - 1]-edgesInsideClusterList[e - 1]) + " -edges. \n");
            }

            
        }//End #6

        /// <summary>
        /// #7 Prints out the edges between different fingerprints
        /// </summary>
        private void outPrintEdges()
        {

            Console.WriteLine("\tEdges between fingerprints : \n");
            for (int i = 0; i <= fingerprintList.Count - 2; i++)
            {

                Console.Write("{0,20}", "\tEdge between ");
                for (int a = i + 1; a < fingerprintList.Count; a++)
                {

                    if (a < 10)
                    {

                        Console.Write("{0,3}{1,1}", "fp", (a + 1) + " ");
                    }
                    else
                    {

                        Console.Write("{0,3}{1,1}", "fp", (a + 1));
                    }
                }
                Console.WriteLine(" ");

                if(i < 10)
                {

                    Console.Write("{0,18}{1,1}", " fp", (i + 1) + " ");
                }
                else
                {

                    Console.Write("{0,18}{1,1}", " fp", (i + 1));
                }

                for (int a = i + 1; a < fingerprintList.Count; a++)
                {
                    Console.Write("{0,5}", merge[i, a]);
                }

                Console.WriteLine(" ");
            }
        }//End 7

 
    }

}
